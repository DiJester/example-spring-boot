package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//import javax.xml.ws.Response;

@SpringBootApplication
@RestController
public class Application {

	@RequestMapping("/")
	public String home() {
		return "Hello Spring Boot";
	}

	@RequestMapping("/hello")
	public String hello(){
		return "Hello world";
	}
	@RequestMapping("/name")
	public String name(@RequestParam String name){
		return "name is "+name;
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
